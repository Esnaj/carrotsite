import logo from "./logo.svg";
import "./App.css";
import Hero from "./pages/hero";
import background from "./carrot_black.jpeg"
import SecondPage from "./pages/secondpage";
import Navbar from "./pages/Navbar";

function App() {
    return (
        <div className="">
            <Navbar />
            <Hero />
            <SecondPage />
        </div>
    );
}

export default App;
