import "./hero.scss";
import logo from "images/carrot.png"

export default function Hero() {
    return (
        <div className="grid justify-items-center hero w-screen h-screen">
            <div className="w-2/3 flex flex-row justify-start items-center">
                <div className="w-1/2 h-1/2 text-white bg-opacity-95 rounded-lg p-10">
                    <h1 className="title">Wortels?</h1>
                    <img src={logo} />
                    <div className="flex-col flex">
                        <span className="text-2xl ">Sinds 1934 verkopen wij de beste wortels ter wereld.</span>
                        <div className="mt-3">
                            <p className="">
                            
                            </p>
                            <div className="mt-3">
                                <button className="px-3 py-2 border-2 border-gray rounded-md shadow-md  cta ">
                                    <div className="innercta">klik hier!</div>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
