import { AnimatePresence, motion } from "framer-motion";
import { ChangeEvent, RefObject, useEffect, useState } from "react";

import { useWindowDimensions } from "./Utils";
import "./navbar.scss";

export default function Navbar(props: { items: [string, RefObject<HTMLDivElement>][] }) {
    const [isActive, setIsActive] = useState(false);
    const { width } = useWindowDimensions();
    console.log(props.items);
    const navbarMotion = {
        initial: { opacity: 0, x: -20 },
        animate: { opacity: 1, x: 0, transition: { duration: 0.22 } },
        exit: { opacity: 0, x: -20, transition: { duration: 0.22 } },
    };
    const buttonMotion = {
        animate: { rotate: 90 },
    };

    useEffect(() => {
        window.addEventListener("scroll", handleScroll);
    }, []);

    const [isScrolled, setIsScrolled] = useState(false);

    const handleScroll = () => {
        const yOffset = window.scrollY;
        if (yOffset > 100) {
            setIsScrolled(true);
        } else {
            setIsScrolled(false);
        }
    };

    if (width > 768) {
        return (
            <nav
                className={[
                    "items-center font-avenir font-semibold flex flex-row visible z-20 py-4 justify-center text-2xl absolute w-screen shadow-lg md:shadow-none px-3 ",
                    isScrolled ? "scrolled text-gray-700" : "notScrolled text-white",
                ].join(" ")}>
                <div className="flex flex-row w-3/4">
                    <NavLinks></NavLinks>
                </div>
            </nav>
        );
    }

    return (
        <div className="fixed top-0 left-0 z-50 text-3xl text-gray-600 font-avenir font-semibold">
            <motion.button
                variants={buttonMotion}
                animate={isActive ? "animate" : ""}
                className="md:hidden absolute z-50 left-5 top-5 w-8 h-8"
                onClick={() => {
                    setIsActive(!isActive);
                }}>
                <img src="../icons/hamburger.svg" className="" alt="hamburger icon" />
            </motion.button>
            <AnimatePresence initial={true}>
                {isActive && (
                    <div>
                        <motion.nav
                            variants={navbarMotion}
                            initial="initial"
                            animate={isActive ? "animate" : "exit"}
                            exit="exit"
                            className={
                                "z-20 absolute  bg-white shadow-xl pt-24  w-40 px-4 h-screen "
                            }>
                            <NavLinks />
                        </motion.nav>
                        <motion.div
                            initial={{ opacity: 0 }}
                            animate={{ opacity: 1 }}
                            exit={{ opacity: 0 }}
                            onClick={() => setIsActive(false)}
                            className="bg-gray-800 z-10 bg-opacity-80 w-screen h-screen"
                        />
                    </div>
                )}
            </AnimatePresence>
        </div>
    );
    function NavLinks() {
        return (
            <ul className="w-full flex flex-col md:flex-row md:space-x-20 font-light z-10 space-y-5 md:space-y-0">
                {props.items.map((e, i) => (
                    <button
                        className="p-0 float-left  navlink"
                        key={i}
                        onClick={() => {
                            e[1].current?.scrollIntoView({ behavior: "smooth", block: "center" });
                            setIsActive(false);
                        }}>
                        {e[0]}
                    </button>
                ))}
            </ul>
        );
    }
}
